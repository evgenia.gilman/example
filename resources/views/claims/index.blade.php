@extends('layouts.base')
@section('content')
    @include('notifications.alerts')

    <h2>{{auth_user()->name}}</h2>

    <h3>All claims</h3>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Book title</th>
            <th scope="col">Return date</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($claims as $claim)
            @if(auth_user()->id == $claim->user_id)

                <tr>
                    <td>{{$claim->book->title}}</td>
                    <td>{{date($claim->date)}}</td>

                    @if($claim->book->status == 'receive')
                        <td>{{$claim->book->status}}</td>
                    @else
                        @if($claim->date <= Carbon\Carbon::now())
                            <td>{{$claim->book->status}}</td>
                        @else
                            <td>expired</td>
                        @endif
                </tr>
            @endif
            @endif

        @endforeach

        </tbody>
    </table>

@endsection
