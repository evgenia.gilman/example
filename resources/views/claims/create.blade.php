@extends('layouts.base')
@section('content')

    <h3>Application form</h3>

    <form enctype="multipart/form-data" method="post" action="{{route('books.claims.store', ['book'=> $book])}}">
        @csrf
        <div class="form-group">
            <label for="library_card">Library card</label>
            <input type="text" class="form-control" id="library_card" name="library_card" value="{{$library_card}}" disabled>
        </div>
        <div class="form-group">
            <input type="hidden" class="form-control" id="book_id" name="book_id">
        </div>
        <div class="form-group">
            <label for="author">Book author</label>
            <input type="text" class="form-control" id="author" name="author" value="{{$book->author}}" disabled>
        </div>
        <div class="form-group">
            <label for="title">Book title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{$book->title}}" disabled>
        </div>
        <div class="form-group">
            <label for="return_date">Return date</label>
            <input type="text" class="form-control border-success  @error('date') is-invalid border-danger
                        @enderror" id="return_date" name="date">
        </div>
        @error('date')
        <p class="text-danger">{{ $message }}</p>
        @enderror
        <button type="submit" class="btn btn-outline-danger">Apply</button>
    </form>

@endsection
