@extends('layouts.base')
@section('content')
    @include('notifications.alerts')


    @foreach($categories as $category)

        <h3>{{$category->title}}</h3>

        <div class="row">
            @foreach($books as $book)

                @if($category->id == $book->category_id)
                    <div class="col">
                        <div class="card mb-3" style="max-width: 540px;">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <img src="{{asset('/storage/' . $book->picture)}}" class="img-thumbnail"
                                         alt="{{asset('/storage/' . $book->picture)}}"
                                         style="width:100px;height:100px;">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$book->author}}</h5>
                                        <h5 class="card-title">{{$book->title}}</h5>
                                    </div>
                                </div>
                                @if($book->status == 'receive')
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <a class="btn btn-primary"
                                               href={{route('books.claims.create', ['book' => $book])}}>Receive</a>
                                        </div>
                                    </div>
                                @elseif($book->status == 'expected')
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <p>Expected</p>
                                            <p>{{date($book->return_date)}}</p>
                                        </div>
                                    </div>

                                @endif
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    @endforeach
    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $books->links('pagination::bootstrap-4') }}
        </div>
    </div>


@endsection
