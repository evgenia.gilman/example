@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Log in</h1>
            <hr>
            <form action="{{route('sessions.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputLibrary_card1">Library card</label>
                    <input type="text" class="form-control" id="exampleInputLibrary_card1" aria-describedby="libraryHelp" name="library_card">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection

