@extends('layouts.base')
@section('content')

    @include('notifications.alerts')

    <h1>Hello, {{auth_user()->library_card}}</h1>
    <form action="{{route('sessions.delete')}}" method="post">
        @csrf
        @method('delete')
        <button type="submit" class="btn btn-sm btn-outline-danger">LOGOUT</button>
    </form>

@endsection
