@extends('layouts.base')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            @include('notifications.alerts')
            <h1 class="text-center">Register</h1>
            <hr>
            <form action="{{route('users.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Name</label>
                    <input type="text" class="form-control" id="exampleInputName1" name="name">
                </div>
                <div class="form-group">
                    <label for="exampleInputAddress1">Address</label>
                    <input type="text" class="form-control" id="exampleInputAddress1" aria-describedby="addressHelp" name="address">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassport1">Passport number</label>
                    <input type="text" class="form-control" id="exampleInputPassport1" aria-describedby="passportHelp" name="passport">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword2">Confirm password</label>
                    <input type="password" class="form-control" id="exampleInputPassword2" name="password_confirmation">
                </div>

                <div class="form-group">
                    <input type="hidden" class="form-control" id="exampleInputLibrary_card2" name="library_card" value="{{random_int(2000000000000000, 8000000000000000)}}">
                </div>

                <button type="submit" class="btn btn-outline-primary">Register</button>
            </form>
        </div>
    </div>
@endsection
