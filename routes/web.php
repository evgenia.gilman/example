<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ClaimsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [CategoriesController::class, 'index']);

Route::resource('page', PagesController::class);

Route::resource('categories', CategoriesController::class)->only('index', 'show');

Route::resource('books.claims', ClaimsController::class);

Route::post('books.claims', [ClaimsController::class, 'store']);

Route::get('/users/register', [UsersController::class, 'register'])->name('users.register');
Route::post('/users', [UsersController::class, 'store'])->name('users.store');


Route::get('/login', [App\Http\Controllers\SessionsController::class, 'create'])->name('sessions.login');
Route::post('/login', [App\Http\Controllers\SessionsController::class, 'store'])->name('sessions.store');
Route::delete('/logout', [App\Http\Controllers\SessionsController::class, 'destroy'])->name('sessions.delete');
