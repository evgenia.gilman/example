<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SessionsController extends AuthController
{
    /**
     * @param Request $request
     * @param User $user
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Request $request, User $user)
    {
        if ($request->session()->exists('user_id')) {
            return redirect()->route('categories.index')->with('error', 'You are already login!');
        }
        return view('sessions.create');
    }

    /**
     * @param SessionRequest $request
     * @return RedirectResponse
     */
    public function store(SessionRequest $request):RedirectResponse
    {
        $user = User::where('library_card', $request->get('library_card'))->firstOrFail();
        if ($this->auth($user, $request->get('password'))) {
            $this->logIn($user);
            return redirect()->route('categories.index')->with('success', 'You are login success');
        } else {
            return back()->with('error', 'Incorrect library_card or password');
        }
    }

    /**
     * @return RedirectResponse
     */
    public function destroy(): RedirectResponse
    {
        $this->logOut();
        return  redirect()->route('sessions.login')->with('status', 'You ae success logout');
    }
}
