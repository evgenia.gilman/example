<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return RedirectResponse
     */
    public function home()
    {
        if(session()->exists('user_id'))
        {
            return  redirect()->route('categories.index');
        }
        return  redirect()->route('sessions.login');
    }
}
