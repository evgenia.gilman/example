<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{


    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = Category::all();
        $books  = Book::paginate(8);

        return view('categories.index', compact('categories', 'books'));
    }


    /**
     * @param Category $category
     * @return Application|Factory|View
     */
    public function show(Category $category)
    {
        return view('categories.show', compact('category'));
    }

}
