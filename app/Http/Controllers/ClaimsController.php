<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClaimRequest;
use App\Models\Book;
use App\Models\Claim;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ClaimsController extends Controller
{

    /**
     * @param Book $book
     * @return Application|Factory|View
     */
    public function index(Book $book)
    {
        $claims = Claim::all();
        return view('claims.index', compact('claims', 'book'));
    }


    /**
     * @param Book $book
     * @return Application|Factory|View
     */
    public function create(Book $book)
    {
        $library_card = auth_user()->library_card;
        return view('claims.create', compact('book','library_card'));
    }

    /**
     * @param ClaimRequest $request
     * @param Book $book
     * @return RedirectResponse
     */
    public function store(ClaimRequest $request, Book $book):RedirectResponse
    {
        $date = $request->input('date');

        if($book->status == 'receive'){
            $book->status = 'expected';
        } else {
            $book->status = 'receive';
        }
        $book->return_date = Carbon::parse($date);
        $book->save();

        $claim= new Claim();
        $claim->user_id = auth_user()->id;
        $claim->book_id = $book->id;
        $claim->date = Carbon::parse($date);
        $claim->save();
        return redirect()->route('categories.index');
    }

}
