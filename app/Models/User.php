<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'address', 'passport', 'password', 'library_card'];

    /**
     * @return HasMany
     */
    public function claims(): HasMany
    {
        return $this->hasMany(Claim::class);
    }
}
