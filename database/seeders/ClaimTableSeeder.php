<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClaimTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Claim::factory()->count(10)->create();
    }
}
